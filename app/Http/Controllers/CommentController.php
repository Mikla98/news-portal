<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use App\Models\Comment;

class CommentController extends Controller
{
    public function create(Request $request)
    {


        $userId = Auth::user()->id;

        Comment::create([
            'comment' => $request->comment,
            'user_id' => $userId,
            'post_id' => $request->postId,
        ]);


        return redirect()->route('post.show',$request->postId);
    }
}
