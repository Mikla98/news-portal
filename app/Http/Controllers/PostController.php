<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Inertia\Inertia;
use App\Models\Post;
use App\Models\Comment;
use App\Models\Vote;

class PostController extends Controller
{
    public function create(Request $request)
    {
 
        $imageName = time().'.'.$request->photo->extension();  
        
        $request->photo->move(public_path('images'), $imageName);



        $userId = Auth::user()->id;

        Post::create([
            'title' => $request->title,
            'body' => $request->body,
            'category' => $request->category,
            'user_id' => $userId,
            'photo' => $imageName,
        ]);


        return redirect()->route('dashboard');
    }

    public function dashboard(){

        $userId = Auth::user()->id;
        $posts = Post::where('user_id', $userId)->get();

        return Inertia::render('Dashboard',['posts' => $posts]);
    }

    public function show($id){

        $post = Post::where('id', $id)->get();  

        $comments = Comment::where('post_id', $id)->with('user')->get();

        $ups = Vote::where('post_id',$id)->where('ups', 1)->get()->count();

        $downs = Vote::where('post_id',$id)->where('downs', 1)->get()->count();

        $color = Session::get('color');

        if(Session::has('message')){

            $message = Session::get('message');

            return Inertia::render('Post',[
                'post' => $post,
                'comments' => $comments,
                'message' => $message,
                'ups' => $ups,
                'downs' => $downs,
                'color' => $color
            ]);
        }else{

            return Inertia::render('Post',[
                'post' => $post,
                'comments' => $comments,
                'ups' => $ups,
                'downs' => $downs,
                'color' => $color
                ]);

        }


    }
}

