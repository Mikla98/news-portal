<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use App\Models\Vote;
use App\Models\Post;
use App\Models\User;
use App\Models\Comment;

class ProfileController extends Controller
{

    public function profile($id){

        $posts = Post::where('user_id',$id)->get();

        $postCount = count($posts);

        $user = User::where('id', $id)->with('followers')->with('following')->first();

        $userFollowers = $user->followers->count();

        $userFollowing = $user->following->count();


        return Inertia::render('Profile',[

            'posts' => $posts,
            'postCount' => $postCount,
            'userFollowers' => $userFollowers,
            'userFollowing' => $userFollowing

        ]);

        

    }

    public function follow($id){

        if($id == Auth::user()->id){

            return redirect()->route('profile',$id);


        }else{

            return redirect()->route('profile',$id);

        }
    }

}
