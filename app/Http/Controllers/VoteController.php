<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use App\Models\Vote;
use App\Models\Post;
use App\Models\Comment;

class VoteController extends Controller
{
    public function up($id){

        $userId = Auth::user()->id;

        $vote = Vote::where('user_id',$userId)->where('post_id',$id)->get();

        if(count($vote) == 0){

            Vote::create([
                'ups' => 1,
                'downs' => 0,
                'user_id' => $userId,
                'post_id' => $id,
            ]);

            return redirect()->route('post.show',$id)->with('message', 'Spremili smo vas glas!')->with('color','green');
            
        }else{

            return redirect()->route('post.show',$id)->with('message', 'Vec zabiljezen glas!')->with('color','red');

        }

        
    }

    public function down($id){

        $userId = Auth::user()->id;

        $vote = Vote::where('user_id',$userId)->where('post_id',$id)->get();

        if(count($vote) == 0){

            Vote::create([
                'ups' => 0,
                'downs' => 1,
                'user_id' => $userId,
                'post_id' => $id,
            ]);

            return redirect()->route('post.show',$id)->with('message', 'Spremili smo vas glas!')->with('color','green');
            
        }else{

            return redirect()->route('post.show',$id)->with('message', 'Vec zabiljezen glas!')->with('color','red');

        }

        
    }
}

