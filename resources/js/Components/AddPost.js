import React, { useEffect, useState } from 'react';
import Input from './Input'
import Label from './Label';
import Button from './Button'
import ListPick from './ListPick'
import ValidationErrors from './ValidationErrors'
import {useForm } from '@inertiajs/inertia-react';  


export default function AddPost() {

    const [message, setMessage] = useState("");

    const { data, setData, post, processing, errors, reset } = useForm({
        title: '',
        body: '',
        photo: null,
        category: '',
    });
    

    useEffect(() => {

        setTimeout(()=>{
            setMessage('');
        },2000)

      }, [message]);




    const submit = (e) => {
        e.preventDefault();

        post(route('post.create'));
        
        setMessage("Nova vijest dodana!");

    };

    let getCategory = (category) => {     
 
    
        setData('category', category);               

    }

    const onHandleChange = (event) => {

        setData(event.target.name, event.target.type === 'checkbox' ? event.target.checked : event.target.value);
        
    };

    useEffect(() => {
        return () => {

            reset('title');
            reset('body');
        };
    }, []);





    return (

 
        <form encType="multipart/form-data"  onSubmit={submit}>
            <p className='text-gray-700 font-extrabold text-2xl'>Dodaj novi clanak</p>
                
            <ValidationErrors errors={errors} />
            {message && <div className="bg-green-400 text-center rounded-lg text-lg font-bold text-white px-4 py-2"><p>{message}</p></div>}

            <Label forInput="title" value="Naslov" className="mt-4" />

            <Input
            type="text"
            name="title"
            className="mt-1 block w-full"
            autoComplete="title"
            isFocused={true}
            handleChange={onHandleChange}
            />

            <Label className="mt-4" forInput="body" value="Opis" />

                <textarea
                rows={4}
                name="body"
                id="body"
                className="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                defaultValue={''}
                autoComplete="body"
                onChange={onHandleChange}
                />

            <Label className="mt-4" forInput="list" value="Kategorija" />

            <ListPick func={getCategory}/>


            <input type="file" name="photo" className="font-bold mt-4" onChange={e => setData('photo', e.target.files[0])} />

           
            <Button className="mt-8" processing={processing}>
                Dodaj vijest
            </Button>
            
        </form>

    )
}
