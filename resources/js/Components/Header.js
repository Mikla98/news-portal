import React, { useEffect, useRef } from 'react';


export default function Header(props){

    const navigation = [
     
    ]

    return (
        <header className="bg-blue-800">
            <nav className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8" aria-label="Top">
                <div className="w-full py-6 flex items-center justify-between lg:border-none">
                <div className="flex items-center">
                    <a href="/">
                    <span className="sr-only">Workflow</span>
                    <img
                        className="h-10 w-auto"
                        src="https://tailwindui.com/img/logos/workflow-mark.svg?color=white"
                        alt=""
                    />
                    </a>
                </div>
                <div className="ml-10 space-x-4">
                {props.user ? (
                    <a
                    href={route('dashboard')}
                    className="inline-block bg-blue-500 py-2 px-4 border border-transparent rounded-md text-base font-medium text-white hover:bg-opacity-75"
                >
                    Dashboard
                </a>
                            ) : (
                                <>
                    <a
                    href={route('login')}
                    className="inline-block bg-blue-500 py-2 px-4 border border-transparent rounded-md text-base font-medium text-white hover:bg-opacity-75"
                    >
                    Prijava
                    </a>
                    <a
                    href={route('register')}
                    className="inline-block bg-white py-2 px-4 border border-transparent rounded-md text-base font-medium text-blue-600 hover:bg-blue-50"
                    >
                   Registracija
                    </a>
                                </>
                            )}
                </div>
                </div>
            </nav>
            </header>
    );
}
