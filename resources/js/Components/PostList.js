import React from 'react'
import { Link } from '@inertiajs/inertia-react'

export default function PostList(props) {

    const posts = props.posts

    return (
        <div className="flex flex-col w-full ">
            <p className='text-gray-700 font-extrabold text-2xl'>Moji clanci</p>
            {posts.map((post,i) => (

            <Link href={"/post/" + post.id} method="get" as="button" type="button" key={i}>
                <div className="relative h-64 my-4" >
                    <img src={'images/'+post.photo} alt={post.title} className="absolute z-10 w-full h-64 rounded-md object-cover object-center"/>
                    <div className="absolute z-20 w-full p-4 h-full bg-gray-800 bg-opacity-30 rounded-md">
                        <p className="text-sm bg-blue-500 inline-block text-center rounded-md px-2 py-1 w-auto text-white font-bold">{post.category}</p>
                        <p className="text-2xl text-white font-bold mt-4">{post.title}</p>
                        <p className="text-lg text-white font-medium mt-1">{post.body}</p>
                    </div>
                    
                    
                </div>
            </Link>
                
            ))}
        </div>
    )
}
