import React from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import { Navigation } from "swiper";
import "swiper/css/navigation";

export default function Slider() {
    const mockData = [
        {
          ranking: 1,
          title: "Naslov",
          link: "/naslov",
          logo: "https://images.unsplash.com/photo-1640457298166-fe3eddec2d5f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2832&q=80",
        },
        {
          ranking: 2,
          title: "Naslov",
          link: "/naslov",
          logo: "https://images.unsplash.com/photo-1640457298166-fe3eddec2d5f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2832&q=80",
        },
        {
          ranking: 3,
          title: "Naslov",
          link: "/naslov",
          logo: "https://images.unsplash.com/photo-1640457298166-fe3eddec2d5f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2832&q=80",
        },
        {
          ranking: 4,
          title: "Naslov",
          link: "/naslov",
          logo: "https://images.unsplash.com/photo-1640457298166-fe3eddec2d5f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2832&q=80",
        },
        {
          ranking: 5,
          title: "Naslov",
          link: "/naslov",
          logo: "https://images.unsplash.com/photo-1640457298166-fe3eddec2d5f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2832&q=80",
        },
      ];
    return (
        <div className='mt-10  max-w-7xl mx-auto'>
            <p className='font-bold text-4xl text-gray-600 py-10'>World News</p>

            <Swiper
                modules={[Navigation]}
                spaceBetween={30}
                slidesPerView={3}
                navigation
                breakpoints={{
                0: {
                    slidesPerView: 1,
                },
                768: {
                    slidesPerView: 2,
                },
                1200: {
                    slidesPerView: 3,
                },
                }}
            >
                {mockData.map((project) => (
                <SwiperSlide key={project.ranking}>
                <img src={project.logo} className='rounded-md'/>
                </SwiperSlide>
                ))}
            </Swiper>

        </div>
    );
}