import React from 'react';
import Authenticated from '@/Layouts/Authenticated';
import { Head } from '@inertiajs/inertia-react';
import AddPost from '../Components/AddPost';
import PostList from '../Components/PostList';
import axios from "axios";

export default function Dashboard(props) {


    
    return (
        <Authenticated
            auth={props.auth}
            errors={props.errors}
        >
            <Head title="Dashboard" />


        <div className='flex flex-col md:flex-row max-w-7xl mx-auto'>

            <div className="py-12 w-full md:w-1/2">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
                    <div className="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                        <div className="p-6 bg-white border-b border-gray-200">

                            <AddPost/>
                            
                        </div>
                    </div>
                </div>
            </div>

            <div className="py-12 w-full md:w-1/2">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
                    <div className="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                        <div className="p-6 bg-white border-b border-gray-200">

                            <PostList posts={props.posts}/>

                        </div>
                    </div>
                </div>
            </div>
        </div>



        </Authenticated>
    );
}
