import React from 'react'
import {useForm } from '@inertiajs/inertia-react'
import { BiCaretUp } from "react-icons/bi";
import { BiCaretDown } from "react-icons/bi";
import { AiFillNotification } from "react-icons/ai";
import { Inertia } from '@inertiajs/inertia';
import Authenticated from '@/Layouts/Authenticated';


export default function Post(props) {

    const postInfo = props.post[0]

    const comments = props.comments


    

    const onHandleChange = (event) => {

        setData(event.target.name, event.target.type === 'checkbox' ? event.target.checked : event.target.value);
        
    };

    const { data, setData, post, processing, errors, reset } = useForm({
        comment: '',
        postId: postInfo.id,
    });

    const submit = (e) => {

        e.preventDefault();

        post(route('comment.create',postInfo.id));
        

    };

    const voteUp = (e) => {

        e.preventDefault();

        Inertia.post(route('vote.up',postInfo.id));
        

    };


    const voteDown = (e) => {

        e.preventDefault();

        Inertia.post(route('vote.down',postInfo.id));
        

    };


    return (

    <Authenticated
    auth={props.auth}
    errors={props.errors}
    >
        
    <div className="flex flex-col items-center p-10 w-3/4 mx-auto relative">
        {props.message && <div className={"bg-"+props.color+"-400 rounded-md absolute z-10 top-10 px-4 py-2 text-white font-bold text-center text-xl"}>{props.message}</div>}
        <div className=" w-3/4 text-left px-4 py-8 flex flex-col items-center justify-between">

            <div className="w-full flex items-center">

                <div className="w-3/4">
                    <p className="text-white text-sm px-2 py-1 bg-blue-500 inline-block rounded-md font-bold ">{postInfo.category}</p>
                    <p className="text-gray-800 text-2xl font-bold mt-4 ">{postInfo.title}</p>
                </div>
                <div className="flex flex-col w-1/4 ">

                    <div className="flex flex-row-reverse items-center">
                        <p className="text-sm inline-block font-bold text-gray-500">{props.ups}</p>
                        <BiCaretUp onClick={voteUp} className="text-4xl text-gray-800"/>
                    </div>

                    <div className="flex flex-row-reverse items-center">
                        <p className="text-sm inline-block font-bold text-gray-500">{props.downs}</p>
                        <BiCaretDown onClick={voteDown} className="text-4xl text-gray-800"/>
                    </div>

                </div>

            </div>


            <div className="w-full mt-10">

                <img src={'../images/'+postInfo.photo} alt={postInfo.title} className="w-full h-96 object-cover rounded-md object-center"/>

                <div className="text-xl text-gray-500 mt-10 px-4 py-6 prose">{postInfo.body}</div>

            </div>

        </div>


        <div className="flex flex-col w-3/4">

        <form className="flex items-center justify-between mt-2 px-2 py-8 space-x-2"  onSubmit={submit}>


            <input type="text" name="comment" placeholder="Comment" className="rounded-md border-0 text-lg py-4 px-4 w-full focus:ring-0" onChange={onHandleChange}/>

            <button className="bg-blue-500 text-white text-lg font-bold px-8 py-5 rounded-md"><AiFillNotification className="text-lg"/></button>

        </form>

            <div className="p-2">

            {comments.map((comment,i) => (


            <div key={i} className=" w-full p-4 bg-gray-50 mt-2 rounded-md">

                <p className="text-xl text-gray-800 font-medium ">{comment.comment}</p>
                <p className="text-sm text-gray-400 font-light mt-4">{comment.user.name}</p>
                

            </div>  
            ))}

            </div>

        </div>




    </div>

    </Authenticated>



    )
}

