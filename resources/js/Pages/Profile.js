import React from 'react'
import Authenticated from '@/Layouts/Authenticated';
import { Link } from '@inertiajs/inertia-react'

export default function Profile(props) {

    const posts = props.posts

    console.log()

    return (

        <Authenticated
        auth={props.auth}
        errors={props.errors}
        >
        <div className="w-4/5 mx-auto flex flex-col md:flex-row  bg-white rounded-md p-6 mt-10">

            <img src='../images/avatar.jpeg' alt="avatar" className=" w-32 h-32 rounded-full object-cover object-center"/>

            <div className="text-xl text-gray-400 ml-12 mt-10">
                <p className="text-gray-800">{props.auth.user.name}</p>
                <p className="mt-2 text-base">{props.auth.user.email}</p>  
                <a className="text-white text-sm font-bold px-4 py-1 bg-green-400 inline-block mt-2 rounded cursor-pointer hover:bg-green-500">Follow</a>
            </div>

            <div className="text-xl text-gray-400 ml-12 mt-10">
                <p className="text-gray-800">Followers</p>
                <p className="mt-2 text-base">{props.userFollowers}</p>  
            </div>

            <div className="text-xl text-gray-400 ml-12 mt-10">
                <p className="text-gray-800">Following</p>
                <p className="mt-2 text-base">{props.userFollowing}</p>  
            </div>

            <div className="text-xl text-gray-400 ml-12 mt-10">
                <p className="text-gray-800">Objave</p>
                <p className="mt-2 text-base">{props.postCount}</p>  
            </div>

        </div>

        <div className="w-4/5 mx-auto flex space-x-4 rounded-md  mt-10">

        {posts.map((post,i) => (

        <Link href={"/post/" + post.id} className="w-full md:w-1/4 text-left" method="get" as="button" type="button" key={i}>
                <div className="relative h-64 my-4" >
                    <img src={'/../images/'+post.photo} alt={post.title} className="absolute z-10 w-full h-64 rounded-md object-cover object-center"/>
                    <div className="absolute z-20 w-full p-4 h-full bg-gray-800 bg-opacity-30 rounded-md">
                        <p className="text-xs bg-blue-500 inline-block text-center rounded-md px-2 py-1 w-auto text-white font-bold">{post.category}</p>
                        <p className="text-2xl text-white font-bold mt-4">{post.title}</p>
                        
                    </div>
                    
                    
                </div>
        </Link>
    
        ))}

        </div>

        </Authenticated>
    )
}
