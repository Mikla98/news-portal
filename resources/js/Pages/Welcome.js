import React from 'react';
import { Link, Head } from '@inertiajs/inertia-react';
import Header from '../Components/Header'
import Slider from '../Components/Slider'
import Footer from '../Components/Footer'

export default function Welcome(props) {
    return (
        <>
            <Head title="MediaRoom" />

            <Header user={props.auth.user}/>

            <div className='max-w-7xl mx-auto mt-32 text-center  '>

                <h2 className='text-5xl text-gray-700 font-extrabold'>Društvena mreža koja spaja sadržaj sa korisnikom.</h2>
                <p className='text-2xl text-gray-500 font-semibold mt-4'>Nemojte čitati sranja. Čitajte šta želite kad želite.</p>
                <a 
                href={route('register')}
                className="inline-block bg-blue-800 py-4 px-8 mt-8 border border-transparent rounded-md text-xl font-semibold text-white hover:bg-opacity-75">
                Registriraj se
                </a>

            </div>

            <div className="bg-blue-800 mt-32">
                <div className="max-w-7xl mx-auto py-12 px-4 sm:py-16 sm:px-6 lg:px-8 lg:py-20">
                    <div className="max-w-4xl mx-auto text-center">
                    <h2 className="text-3xl font-extrabold text-white sm:text-4xl">U svakom trenutku provjerite najnovije podatke</h2>
                    <p className="mt-3 text-xl text-blue-200 sm:mt-4">
                        Velicina platforme se mjeri brojkama, pa smo odlucili da brojke od pocetka budu transparentne i kad nama neidu u korist.
                    </p>
                    </div>
                    <dl className="mt-10 text-center sm:max-w-3xl sm:mx-auto sm:grid sm:grid-cols-3 sm:gap-8">
                    <div className="flex flex-col">
                        <dt className="order-2 mt-2 text-lg leading-6 font-medium text-blue-200">Korisnici</dt>
                        <dd className="order-1 text-5xl font-extrabold text-white">92</dd>
                    </div>
                    <div className="flex flex-col mt-10 sm:mt-0">
                        <dt className="order-2 mt-2 text-lg leading-6 font-medium text-blue-200">Clanci</dt>
                        <dd className="order-1 text-5xl font-extrabold text-white">240</dd>
                    </div>
                    <div className="flex flex-col mt-10 sm:mt-0">
                        <dt className="order-2 mt-2 text-lg leading-6 font-medium text-blue-200">Glasovi</dt>
                        <dd className="order-1 text-5xl font-extrabold text-white">1280</dd>
                    </div>
                    </dl>
                </div>
                </div>

            <div className='max-w-7xl mx-auto mt-20 flex flex-col md:flex-row p-4 md:p-10  '>
                
                <div className='w-full md:w-1/2 flex flex-col items-start justify-start px-0 md:px-8'>
                    <h3 className='text-4xl text-gray-700 font-extrabold'>Kako funkcionira?</h3>
                    <p className='text-xl text-gray-500 font-semibold mt-4 leading-relaxed'>Prvo je potrebna registracija. Nakon registracije korisnik ima svoj profil na kojem moze objavljivati clanke. Korisnik moze pratiti korisnike i citati njiove clanke. Dosta jednostavno , zar ne? </p>
                </div>

                <div className='w-full md:w-1/2 mt-8 md:mt-0 '>
                 <img src="img/home.jpg" className='rounded-md'/>
                </div>

            </div>

            <div className='max-w-7xl mx-auto mt-20 flex flex-col-reverse md:flex-row p-4 md:p-10  '>

                <div className='w-full md:w-1/2 mt-8 md:mt-0 '>
                    <img src="img/home.jpg" className='rounded-md'/>
                </div>

                <div className='w-full md:w-1/2 flex flex-col items-start justify-start px-0 md:px-8'>
                    <h3 className='text-4xl text-gray-700 font-extrabold'>Sustav glasovanja</h3>
                    <p className='text-xl text-gray-500 font-semibold mt-4 leading-relaxed'>Svaki clanak moze biti dobar, a moze biti tesko sranje. Klasika. Zato smo mi implementirali sustav glasovanja kojim svaki korisnik moze jednom glasati na clanak. Glas moze biti pozitivan, a moze biti negativan. Korisnik moze vidjeti ukupan broj glasova, cime zajednica pokazuje svoje djelovanje. </p>
                </div>

            </div>

            <div className='max-w-7xl mx-auto mt-20 flex flex-col md:flex-row p-4 md:p-10  '>
                
                <div className='w-full md:w-1/2 flex flex-col items-start justify-start px-0 md:px-8'>
                    <h3 className='text-4xl text-gray-700 font-extrabold'>Zasto smo ovo napravili?</h3>
                    <p className='text-xl text-gray-500 font-semibold mt-4 leading-relaxed'>Mediji pisu svasta i ljudi citaju svasta. Jako puno placenih clanaka, jako malo ciste istine. Vrijeme je da svatko pise, a ne samo komentira na portalima. </p>
                </div>

                <div className='w-full md:w-1/2 mt-8 md:mt-0 '>
                 <img src="img/home.jpg" className='rounded-md'/>
                </div>

            </div>


            <Footer />


        </>
    );
}
