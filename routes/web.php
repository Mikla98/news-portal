

<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\VoteController;
use App\Http\Controllers\ProfileController;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::get('/profile', function () {
    return Inertia::render('Profile');
})->middleware('auth')->name('profile');

Route::get('/dashboard', [PostController::class, 'dashboard'])
                ->middleware(['auth', 'verified'])
                ->name('dashboard');

Route::get('/profile/{id}', [ProfileController::class, 'profile'])
                ->middleware(['auth', 'verified'])
                ->name('profile');

Route::post('/profile/{id}/follow', [ProfileController::class, 'follow'])
                ->middleware(['auth', 'verified'])
                ->name('profile.follow');

Route::post('/post/create', [PostController::class, 'create'])
                ->middleware('auth')
                ->name('post.create');  

Route::get('/post/{id}', [PostController::class, 'show'])
                ->middleware('auth')
                ->name('post.show');


Route::post('/post/{id}/comment/create', [CommentController::class, 'create'])
                ->middleware('auth')
                ->name('comment.create');

Route::post('/post/{id}/vote/up', [VoteController::class, 'up'])
                ->middleware('auth')
                ->name('vote.up');

Route::post('/post/{id}/vote/down', [VoteController::class, 'down'])
                ->middleware('auth')
                ->name('vote.down');

require __DIR__.'/auth.php';

